<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('options.create-my-address', compact('customer_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $me = new \App\MyAddresses;
        $me->companyName  = $request->companyName;
        $me->contactName  = $request->contactName;
        $me->addrLine1    = $request->addrLine1;
        $me->addrLine2    = $request->addrLine2;
        $me->country      = $request->country;
        $me->city         = $request->city;
        $me->state        = $request->state;
        $me->zip          = $request->zip;

        $me->save();

        return redirect('options.my-addresses')->with('success', 'Daten wurden gespeichert!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $me = \App\MyAddresses::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $me = \App\MyAdresses::find($id);

        $me->companyName  = $request->companyName;
        $me->contactName  = $request->contactName;
        $me->addrLine1    = $request->addrLine1;
        $me->addrLine2    = $request->addrLine2;
        $me->country      = $request->country;
        $me->city         = $request->city;
        $me->state        = $request->state;
        $me->zip          = $request->zip;
        $me->save();

        return redirect('my-address')->with('success', 'Daten wurden gespeichert!');
    }

    public function delete($id)
    {
        $me = \App\MyAddresses::find($id);
        return view('options.delete-my-address', compact('me'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $me = \App\MyAddresses::find($id);
        $me->delete();

        return redirect('options');
    }
}
