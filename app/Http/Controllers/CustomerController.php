<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = \App\Customer::all();
        return view('customer.index', ['customers' => $customers]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = \App\Customer::max('customerId');
        if (!$customer) {
            $customer_id = 1;
        } else {
            $customer_id = intval($customer) + 1;
        }
        return view('customer.create', compact('customer_id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $customer = new \App\Customer;
        $customer->companyName  = $request->companyName;
        $customer->contactName  = $request->contactName;
        $customer->addrLine1    = $request->addrLine1;
        $customer->addrLine2    = $request->addrLine2;
        $customer->country      = $request->country;
        $customer->city         = $request->city;
        $customer->state        = $request->state;
        $customer->zip          = $request->zip;
        $customer->customerId   = $request->customerId;
        $customer->customerVAT  = $request->customerVAT;

        $customer->save();

        return redirect('customers')->with('success', 'Daten wurden gespeichert!');

    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $customer = \App\Customer::find($id);

        return response()->json(array('success' => true, 'customer' => $customer));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //grab customer data
        $customer = \App\Customer::find($id);
        return view('customer.edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //save customer data
        $customer = \App\Customer::find($id);

        $customer->companyName  = $request->companyName;
        $customer->contactName  = $request->contactName;
        $customer->addrLine1    = $request->addrLine1;
        $customer->addrLine2    = $request->addrLine2;
        $customer->country      = $request->country;
        $customer->city         = $request->city;
        $customer->state        = $request->state;
        $customer->zip          = $request->zip;
        $customer->customerId   = $request->customerId;
        $customer->customerVAT  = $request->customerVAT;

        $customer->save();

        return redirect('customers')->with('success', 'Daten wurden gespeichert!');
    }

    public function delete($id)
    {
        $customer = \App\Customer::find($id);
        return view('customer.delete', compact('customer'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $customer = \App\Customer::find($id);
        $customer->delete();

        return redirect('customers');
    }
}
