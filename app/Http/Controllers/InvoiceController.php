<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Konekt\PdfInvoice\InvoicePrinter;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $invoices = \App\Invoice::all();
        return view('invoice.index', ['invoices' => $invoices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           
        $customers = \App\Customer::all();
        $invoice = \App\Invoice::max('reference');
        if (!$invoice) {
            $invoice_id = 1;
        } else {
            $invoice_id = intval($invoice) + 1;
        }
        return view('invoice.create', compact('customers', 'invoice_id'));
    }

    public function render(Request $request, $id)
    {
        // $invoice = new InvoicePrinter('A4','€','de');
        
        // /* Header settings */
        // $invoice->setLogo(public_path()."/img/PPWM_Logo.jpg");   //logo image path
        // $invoice->setColor("#000");      // pdf color scheme
        // $invoice->setType("Rechnung");    // Invoice Type
        // $invoice->setReference("INV-55033645");   // Reference
        // $invoice->setDate(date('M dS ,Y',time()));   //Billing Date
        // // $invoice->setTime(date('h:i:s A',time()));   //Billing Time
        // // $invoice->setDue(date('M dS ,Y',strtotime('+3 months')));    // Due Date
        // $invoice->setFrom(array("Seller Name","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740"));
        // $invoice->setTo(array("Purchaser Name","Sample Company Name","128 AA Juanita Ave","Glendora , CA 91740"));
        
        // $invoice->addItem("AMD Athlon X2DC-7450","2.4GHz/1GB/160GB/SMP-DVD/VB",6,0,580,0,3480);
        // $invoice->addItem("PDC-E5300","2.6GHz/1GB/320GB/SMP-DVD/FDD/VB",4,0,645,0,2580);
        // $invoice->addItem('LG 18.5" WLCD',"",10,0,230,0,2300);
        // $invoice->addItem("HP LaserJet 5200","",1,0,1100,0,1100);
        
        // $invoice->addTotal("Total",9460);
        // $invoice->addTotal("VAT 21%",1986.6);
        // $invoice->addTotal("Total due",11446.6,true);
        
        // // $invoice->addBadge("Payment Paid");
        
        // $invoice->addTitle("Important Notice");
        
        // $invoice->addParagraph("Reverse Charge System - VAT due to the recipient.");
        // $invoice->addParagraph("Terms of payment: Net within 90 days of invoice date.");
        
        // $invoice->setFooternote("PPW Media e.U");
        
        // $invoice->render('example1.pdf','I'); 

        //RENDER TEST

        $invoice = \App\Invoice::find($id);

        $translations = [
            'en' => [
                'total' => 'Total',
                'tax' => 'VAT',
                'title' => 'Invoice',
                'notice' => 'Important Notice',
                'conditions' => 'Terms of payment: Net within '.$invoice->days_to_pay.' days of invoice date.',
                'total_due' => 'Total due'
                ],
            'de' => [
                'total' => 'Gesamt netto',
                'tax' => 'MwSt',
                'title' => 'Rechnung',
                'notice' => 'Wichtige Mitteilung',
                'conditions' => 'Zahlungsbedingungen: Zahlbar innerhalb '.$invoice->days_to_pay.' Tagen nach Verrechnung netto ohne Abzug.',
                'total_due' => 'Gesamtsumme',
                ]
            ];

        $locale = $invoice->language;
        $loc = $translations[$locale];

        

        $render = new InvoicePrinter('A4',$invoice->currency, $invoice->language);
        
        /* Header settings */
        $render->setLogo(public_path()."/img/PPWM_Logo.jpg");   //logo image path
        $render->setColor("#000");      // pdf color scheme
        $render->setType($loc['title']);    // Invoice Type
        $render->setReference($invoice->reference);   // Reference
        $date = \DateTime::createFromFormat('d-m-Y', $invoice->billingDate)->format('Y-m-d');
        $render->setDate(date('d M Y', strtotime($invoice->billingDate)));   //Billing Date
        // $render->setTime(date('h:i:s A',time()));   //Billing Time
        // $render->setDue(date('M dS ,Y',strtotime('+3 months')));    // Due Date
        $from = json_decode($invoice->from);
        $to = json_decode($invoice->to);

        $render->setFrom([$from->contact, $from->company, $from->addrLine1, $from->addrLine2, $from->zip.", ".$from->city, $from->country]);
        $render->setTo([$to->contact, $to->company, $to->addrLine1, $to->addrLine2, $to->zip.", ".$to->city, $to->country]);
        
        foreach (json_decode($invoice->items) as $item) {
            $render->addItem($item->product, $item->description , $item->qty, $item->tax, $item->price, $item->red, $item->total);
        }
        
        $render->addTotal($loc['total'],$invoice->subTotal);
        $render->addTotal($loc['tax']." ".$invoice->tax."%",$invoice->taxDue);
        $render->addTotal($loc['total_due'],$invoice->totalDue,true);
        
        // $render->addBadge("Payment Paid");
        
        $render->addTitle($loc['notice']);
        $render->addParagraph($invoice->notices);
        $render->addParagraph($loc['conditions']);
        
        $render->setFooternote("PPW Media e.U");
        
        $name = "Rechnung ".$invoice->reference." ".$to->company;

        $render->render($name,'I');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoice = new \App\Invoice;
        $invoice->reference = $request->reference;
        $invoice->billingDate = $request->date;
        //from
        $from = [
            "contact" => $request->fromContactName, 
            "company" => $request->fromCompanyName, 
            "addrLine1" => $request->fromAddrLine1,
            "addrLine2" => $request->fromAddrLine2,
            "city" => $request->fromCity,
            "zip" => $request->fromZip,
            "country" => $request->fromCountry ];
        //to
        $to = [
            "contact" => $request->toContactName, 
            "company" => $request->toCompanyName, 
            "addrLine1" => $request->toAddrLine1,
            "addrLine2" => $request->toAddrLine2,
            "city" => $request->toCity,
            "zip" => $request->toZip,
            "country" => $request->toCountry ];    
        //item table
        $arraySize = sizeof($request->qty);
        
        $items = [];

        for($i=0; $i < $arraySize; $i++) {
            $items[$i] = [
                'product' => $request->product[$i],
                'description' => $request->description[$i],
                'qty' => $request->qty[$i],
                'tax' => false,
                'price' => $request->price[$i],
                'red' => false,
                'total' => $request->total[$i]
            ];
        }

        $invoice->from = json_encode($from);
        $invoice->to = json_encode($to);
        $invoice->items = json_encode($items);

        //Totals

        $invoice->subTotal = $request->sub_total;
        $invoice->tax = $request->tax;
        $invoice->taxDue = $request->tax_amount;
        $invoice->totalDue = $request->total_amount;
        $invoice->days_to_pay = $request->days_to_pay;

        $invoice->language = $request->language;
        $invoice->currency = $request->currency;

        $invoice->notices = $request->notices;


        $invoice->save();

         


        // return view('invoice.index', ['invoices' => $invoices, 'data' => $request]);
        return redirect('invoices')->with('success', 'Neue Rechnung Erstellt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = \App\Customer::all();
        $invoice = \App\Invoice::find($id);
        return view('invoice.edit', compact('customers', 'invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoice = \App\Invoice::find($id);
        $invoice->reference = $request->reference;
        
        $invoice->billingDate = $request->date;
        //from
        $from = [
            "contact" => $request->fromContactName, 
            "company" => $request->fromCompanyName, 
            "addrLine1" => $request->fromAddrLine1,
            "addrLine2" => $request->fromAddrLine2,
            "city" => $request->fromCity,
            "zip" => $request->fromZip,
            "country" => $request->fromCountry ];
        //to
        $to = [
            "contact" => $request->toContactName, 
            "company" => $request->toCompanyName, 
            "addrLine1" => $request->toAddrLine1,
            "addrLine2" => $request->toAddrLine2,
            "city" => $request->toCity,
            "zip" => $request->toZip,
            "country" => $request->toCountry ];    
        //item table
        $arraySize = sizeof($request->qty);
        
        $items = [];

        for($i=0; $i < $arraySize; $i++) {
            $items[$i] = [
                'product' => $request->product[$i],
                'description' => $request->description[$i],
                'qty' => $request->qty[$i],
                'tax' => false,
                'price' => $request->price[$i],
                'red' => false,
                'total' => $request->total[$i]
            ];
        }

        $invoice->from = json_encode($from);
        $invoice->to = json_encode($to);
        $invoice->items = json_encode($items);

        //Totals

        $invoice->subTotal = $request->sub_total;
        $invoice->tax = $request->tax;
        $invoice->taxDue = $request->tax_amount;
        $invoice->totalDue = $request->total_amount;

        $invoice->language = $request->language;
        $invoice->currency = $request->currency;
        $invoice->days_to_pay = $request->days_to_pay;

        $invoice->notices = $request->notices;

        $invoice->save();

        return redirect('invoices')->with('success', 'Daten wurden aktualisiert.');
    }


    public function delete($id)
    {
        $invoice = \App\Invoice::find($id);
        return view('invoice.delete', compact('invoice'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = \App\Invoice::find($id);
        $invoice->delete();
        return redirect('invoices')->with('success', 'Daten wurden gelöscht.');
    }
}
