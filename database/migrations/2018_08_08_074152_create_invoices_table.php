<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->nullable();
            $table->json('from')->nullable();
            $table->json('to')->nullable();
            $table->json('items');
            $table->string('billingDate');
            $table->decimal('subTotal', 8, 2);
            $table->decimal('taxDue', 8, 2);
            $table->integer('tax');
            $table->decimal('totalDue', 8, 2);
            $table->string('language');
            $table->string('currency');
            $table->string('notices')->nullable();
            $table->integer('days_to_pay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
