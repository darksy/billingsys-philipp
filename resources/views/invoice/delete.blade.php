@extends('layouts.app')

@section('content')


<div class="container">
    <div class="card">
        <div class="card-header">
            Rechnung Löschen
        </div>
        <div class="card-body">
            <div class="alert alert-warning">
                Möchten sie diese Rechnung wirklich löschen?
                <form action="{{action('InvoiceController@destroy', $invoice->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-default" type="submit">Ja, Bitte Löschen</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection