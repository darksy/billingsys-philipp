
@extends('layouts.app')

@section('content')

<div class="container">
    @if (\Session::has('success'))
        <div class="alert alert-sucess">
            <p>{{ \Session::get('success') }}</p>
        </div>
    @endif
    @if(count($invoices) == 0)
    <div class="card border-warning text-center text-warning">
        <div class="card-header">Fehler</div>
        <div class="card-body">
            <p>Es wurden keine Rechnungen in der Datenbank gefunden. <a href="/invoices/create">Neue Rechnung erstellen?</a></p>
        </div>
    </div>

    @else
    <table class="table table-striped">
        <thead>
            <tr>
                <td>Rechnungs Nr.</td>
                <td>Firma</td>
                <td>Kontakt Person</td>
                <td>Aktionen</td>
            </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
        @php
        $to = json_decode($invoice->to)
        @endphp
            <tr> 
                <td>{{$invoice->id}}</td> 
                <td>{{$to->contact}}</td>
                <td>{{$to->company}}</td>
                <td>
                    <a href="{{action('InvoiceController@edit', $invoice->id)}}"><i class="far fa-edit"></i></a>
                    <a href="{{action('InvoiceController@delete', $invoice->id)}}"><i class="far fa-trash-alt"></i></a>
                    <a href="{{action('InvoiceController@render', $invoice->id)}}"><i class="fas fa-file-pdf"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>    
    </table>
    @endif
</div>

@endsection