@extends('layouts.app')

@section('content')

@php

$date = new DateTime('now');

@endphp

@php 
$from = json_decode($invoice->from); 
$to = json_decode($invoice->to);
@endphp
<div class="container">
    
    <div class="card">
        <div class="card-header">
        <h4>Rechnung Erstellen</h4>
        </div>
        {{$invoice->language}}
        <div class="card-body">
            <form method="POST" action="{{action('InvoiceController@update', $invoice->id)}}">
            @method('PATCH')
            {{csrf_field()}}
                <div class="form-row">
                    <div class="col-5">
                        <div class="form-group">
                            <label for="language">Sprache</label>
                            <select class="form-control" name="language" id="">
                                @if ($invoice->language == "en")
                                <option value="en" selected>Englisch</option>
                                <option value="de">Deutsch</option>
                                @else
                                <option value="en" >Englisch</option>
                                <option value="de" selected>Deutsch</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="currency">Währung</label>
                            <select class="form-control" name="currency" id="currency-select">
                                <option value="{{$invoice->currency}}" selected="selected">{{$invoice->currency}}</option>
                                <option value="€">€</option>
                                <option value="$">$</option>
                                <option value="£">£</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-5 offset-2">
                        <div class="form-group">
                            <label for="date">Rechnungs Datum</label>
                            <input type="text" name="date" class="form-control" value="{{$invoice->billingDate}}">
                        </div>
                        <div class="form-group">
                            <label for="reference">Rechnungsnummer</label>
                            <input name="reference" type="text" class="form-control" value="{{$invoice->reference}}">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="author">Rechnung Von</label>
                        <select class="form-control" name="" id="">
                            <option value="test">Wokurka</option>
                        </select>
                    </div>
                    <div class="form-group col-md-5 offset-2">
                        <label for="recipient">Rechnungsempfänger</label>
                        <select class="form-control" name="" id="recipient">
                            @foreach($customers as $customer)
                            @if ($customer->companyName == $to->company)
                            <option value="{{$customer->id}}" selected>{{$customer->companyName}}</option>
                            @else
                            <option value="{{$customer->id}}">{{$customer->companyName}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-4">
                        <div class="form-group">
                            <input type="text" value="{{$from->company}}" class="form-control" id="fromCompanyName" placeholder="Muster GmbH" name="fromCompanyName">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->contact}}" class="form-control" id="fromContactName" placeholder="Konktakt Person" name="fromContactName">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->addrLine1}}" class="form-control" id="fromAddrLine1" placeholder="Adresse 1" name="fromAddrLine1">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->addrLine2}}" class="form-control" id="fromAddrLine2" placeholder="Adresse 2" name="fromAddrLine2">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->zip}}" class="form-control" id="fromZip" placeholder="PLZ" name="fromZip">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->city}}" class="form-control" id="fromCity" placeholder="Stadt" name="fromCity">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$from->country}}" class="form-control" id="fromCountry" placeholder="Land" name="fromCountry">
                        </div>
                    </div>
                    <div class="col-4 offset-4">
                        <div class="form-group">
                            <input type="text"  value="{{$to->company}}" class="form-control" id="toCompanyName" placeholder="Muster GmbH" name="toCompanyName">
                        </div>
                        <div class="form-group">
                            <input type="text"  value="{{$to->contact}}" class="form-control" id="toContactName" placeholder="Konktakt Person" name="toContactName">
                        </div>
                        <div class="form-group">
                            <input type="text"  value="{{$to->addrLine1}}" class="form-control" id="toAddrLine1" placeholder="Adresse 1" name="toAddrLine1">
                        </div>
                        <div class="form-group">
                            <input type="text"  value="{{$to->addrLine2}}" class="form-control" id="toAddrLine2" placeholder="Adresse 2" name="toAddrLine2">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$to->zip}}"  class="form-control" id="toZip" placeholder="PLZ" name="toZip">
                        </div>
                        <div class="form-group">
                            <input type="text" value="{{$to->city}}"  class="form-control" id="toCity" placeholder="Stadt" name="toCity">
                        </div>
                        <div class="form-group">
                            <input type="text"  value="{{$to->country}}" class="form-control" id="toCountry" placeholder="Land" name="toCountry">
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-12">
                        <table class="table table-hover" id="tab_logic">
                            <thead>
                            <tr>
                                <th class="text-center"> # </th>
                                <th class="text-center"> Produkt </th>
                                <th class="text-center"> Beschreibung </th>
                                <th class="text-center"> Menge </th>
                                <th class="text-center"> Preis </th>
                                <th class="text-center"> Gesamtbetrag </th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0 @endphp
                            @foreach(json_decode($invoice->items) as $item)
                            <tr id='addr{{$i}}'>
                                <td>1</td>
                                <td><input type="text" name='product[]' value='{{$item->product}}' placeholder='Enter Product Name' class="form-control"/></td>
                                <td><input type="text" name='description[]' value='{{$item->description}}' placeholder='Enter Description.' class="form-control"/></td>
                                <td><input type="number" name='qty[]' value='{{$item->qty}}' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>
                                <td><input type="number" name='price[]' value='{{$item->price}}' placeholder='Enter Unit Price' class="form-control price" step="0.01" min="0"/></td>
                                <td><input type="number" name='total[]' value='{{$item->total}}' placeholder='0.00' class="form-control total" readonly/></td>
                            </tr>
                            @php $i++ @endphp
                            @endforeach
                            <tr id='addr{{$i}}'></tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12">
                        <button id="add_row" class="btn btn-info pull-left">Reihe Hinzufügen</button>
                        <button id='delete_row' class="pull-right btn btn-warning">Reihe Löschen</button>
                        </div>
                    </div>
                    <div class="row clearfix" style="margin-top:20px">
                        <div class="pull-right col-md-4">
                        <table class="table table-hover" id="tab_logic_total">
                            <tbody>
                            <tr>
                                <th class="text-center">Sub Total</th>
                                <td class="text-center">
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-prepend"><span class="input-group-text input-group-currency" id="currency-display">€</span></div>
                                    <input value="{{$invoice->subTotal}}" type="number" name='sub_total' step='0.01' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                                </div>
                            </tr>
                            <tr>
                                <th class="text-center">Steuer Prozent</th>
                                <td class="text-center">
                                    <div class="input-group mb-2 mb-sm-0">
                                        <input value="{{$invoice->tax}}" type="number" name="tax" class="form-control" id="tax" placeholder="0">
                                        <div class="input-group-append"><span class="input-group-text" id="basic-addon2">%</span></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Steuern Absolut</th>
                                <td class="text-center">
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-prepend"><span class="input-group-text input-group-currency" id="currency-display">€</span></div>
                                    <input value="{{$invoice->taxDue}}" type="number" name='tax_amount' id="tax_amount" step='0.01' placeholder='0.00' class="form-control" readonly/>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <th class="text-center">Gesamtbetrag</th>
                                <td class="text-center">
                                <div class="input-group mb-2 mb-sm-0">
                                    <div class="input-group-prepend"><span class="input-group-text input-group-currency" id="currency-display">€</span></div>
                                    <input value="{{$invoice->totalDue}}" type="number" name='total_amount' id="total_amount" step='0.01' placeholder='0.00' class="form-control" readonly/>
                                </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row" style="margin-bottom:20px">
                    <div class="input-group col">
                        <textarea name="notices" id="notices" class="form-control" placeholder="Bemerkungen" value="">{{$invoice->notices}}</textarea>
                    </div>
                    <div class="input-group col-4">
                        <label for="days_to_pay">Zahlungsfrist</label>
                        <select name="days_to_pay" class="form-control" id="days_to_pay">
                            <option selected="selected" value="{{$invoice->days_to_pay}}">{{$invoice->days_to_pay}} Tage</option>
                            <option value="14">14 Tage</option>
                            <option value="30">30 Tage</option>
                            <option value="90">90 Tage</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Speichern</button>
            </form>
        </div>
    </div>
    
</div>
<script>

$(document).ready(function(){

    var i={{$i}};

    $('#recipient').change(function(){
        var id = $(this).val();
        $.get( "/customers/"+id, function( data ) {
            if (data.success == true) {
                $("#toCompanyName").val(data.customer.companyName);
                $("#toContactName").val(data.customer.contactName);
                $("#toAddrLine1").val(data.customer.addrLine1);
                $("#toAddrLine2").val(data.customer.addrLine2);
                $("#toZip").val(data.customer.zip);
                $("#toCity").val(data.customer.city);
                $("#toCountry").val(data.customer.country);
            }
        });
    });

    $("#add_row").click(function(event){
        event.preventDefault();
        b=i-1;
      	$('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
      	$('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      	i++; 
  	});
    $("#delete_row").click(function(event){
        event.preventDefault();
    	if(i>1){
		$("#addr"+(i-1)).html('');
		i--;
		}
		calc();
	});
	
	$('#tab_logic tbody').on('keyup change',function(){
		calc();
	});
	$('#tax').on('keyup change',function(){
		calc_total();
	});

    $('#currency-select').change(function() {
        $('.input-group-currency').text($(this).val());
    });
	

});

function calc()
{
	$('#tab_logic tbody tr').each(function(i, element) {
		var html = $(this).html();
		if(html!='')
		{
			var qty = $(this).find('.qty').val();
			var price = $(this).find('.price').val();
			$(this).find('.total').val(qty*price);
			
			calc_total();
		}
    });
}

function calc_total()
{
	total=0;
	$('.total').each(function() {
        total += parseFloat($(this).val());
    });
	$('#sub_total').val(total.toFixed(2));
	tax_sum=total/100*$('#tax').val();
	$('#tax_amount').val(tax_sum.toFixed(2));
	$('#total_amount').val((tax_sum+total).toFixed(2));
}

// $(document).ready(function () {
//     var counter = 0;

//     $("#addrow").on("click", function () {
//         var newRow = $("<tr>");
//         var cols = "";

//         cols += '<td><textarea class="form-control" name="description' + counter + '"></textarea></td>';
//         cols += '<td><input type="number" class="form-control price-in" name="price' + counter + '"/></td>';
//         cols += '<td><input type="number" class="form-control amount-in" name="amount' + counter + '"/></td>';
//         cols += '<td><input type="number" class="form-control total-in" name="total' + counter + '" readonly/></td>';

//         cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Löschen"></td>';
//         newRow.append(cols);
//         $("table.order-list").append(newRow);
//         counter++;
//     });



//     $("table.order-list").on("click", ".ibtnDel", function (event) {
//         $(this).closest("tr").remove();       
//         counter -= 1
//     });

//     $("table.order-list").on( 'keyup', '.price-in, .amount-in', function(){
//         // event.stopPropagation();
//         var total = $('.price-in').val()*$('.amount-in').val();
//         $(this).closest("tr").find('.total-in').val(total);
//     });


// });



// function calculateRow(row) {
//     var price = +row.find('input[name^="price"]').val();

// }

// function calculateGrandTotal() {
//     var grandTotal = 0;
//     $("table.order-list").find('input[name^="price"]').each(function () {
//         grandTotal += +$(this).val();
//     });
//     $("#grandtotal").text(grandTotal.toFixed(2));
// }

</script>
@endsection
