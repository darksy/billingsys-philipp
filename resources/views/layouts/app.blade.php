<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laravel Quickstart - Basic</title>

        <!-- CSS And JavaScript -->
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('css/all.css') }}">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-light">
            <a class="navbar-brand" href="/"><img src="{{URL::asset('img/PPWM_Logo_test_png.png')}}" class="img-rounded" style="max-width:100px;" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown2" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Rechnungen</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2" x-placement="bottom-start">
                        <a class="dropdown-item" href="/invoices">Rechnungs Übersicht</a>
                        <a class="dropdown-item" href="/invoices/create">Rechnung Erstellen</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kunden</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" x-placement="bottom-start">
                        <a class="dropdown-item" href="/customers">Kunden Übersicht</a>
                        <a class="dropdown-item" href="/customers/create">Kunde Erstellen</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Einstellungen <i class="fas fa-cog"></i></a>
                    </div>
                </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Navbar Contents -->
            </nav>
        </div>

        @yield('content')
        
    </body>
</html>