@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-header">
                <h4>Adresse Erstellen</h4>
            </div>
            <div class="card-body">
                <form action="/my-address" method="POST">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="companyName">Firma</label>
                            <input type="text" class="form-control" id="companyName" placeholder="Muster GmbH" name="companyName">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contactName">Kontakt Person</label>
                            <input type="text" class="form-control" id="contactName" placeholder="Max Mustermann" name="contactName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="addrLine1">Addrese</label>
                        <input type="text" class="form-control" id="addrLine1" placeholder="1234 Main St" name="addrLine1">
                    </div>
                    <div class="form-group">
                        <label for="addrLine2">Addresse 2</label>
                        <input type="text" class="form-control" id="addrLine2" placeholder="Apartment, studio, or floor" name="addrLine2">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">Stadt</label>
                            <input type="text" class="form-control" placeholder="Osterwald" id="inputCity" name="city">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">PLZ</label>
                            <input type="text" class="form-control" placeholder="00000" id="inputZip" name="zip">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputState">Bundesland</label>
                            <input type="text" class="form-control" placeholder="Musterland" id="inputState" name="state">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputCountry">Land</label>
                            <input type="text" class="form-control" placeholder="Land" id="inputCountry" name="country">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Speichern</button>
                </form>
            </div>
        </div>

    </div>

@endsection
