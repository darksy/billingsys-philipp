@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-header">
                <h4>Addresse Bearbeiten</h4>
            </div>
            <div class="card-body">
                <form method="POST" action="/my-address/update">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="companyName">Firma</label>
                            <input type="text" class="form-control" id="companyName" value="{{$myAdress->companyName}}" name="companyName">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contactName">Kontakt Person</label>
                            <input type="text" class="form-control" id="contactName" value="{{$myAdress->contactName}}" name="contactName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="addrLine1">Addrese</label>
                        <input type="text" class="form-control" id="addrLine1" value="{{$myAdress->addrLine1}}" name="addrLine1">
                    </div>
                    <div class="form-group">
                        <label for="addrLine2">Addresse 2</label>
                        <input type="text" class="form-control" id="addrLine2" value="{{$myAdress->addrLine2}}" name="addrLine2">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">Stadt</label>
                            <input type="text" class="form-control" value="{{$myAdress->city}}" id="inputCity" name="city">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">PLZ</label>
                            <input type="text" class="form-control" value="{{$myAdress->zip}}" id="inputZip" name="zip">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputState">Bundesland</label>
                            <input type="text" class="form-control" value="{{$myAdress->state}}" id="inputState" name="state">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputCountry">Land</label>
                            <input type="text" class="form-control" value="{{$myAdress->country}}" id="inputCountry" name="country">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Speichern</button>
                </form>
            </div>
        </div>

    </div>

@endsection
