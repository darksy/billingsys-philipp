@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="card">
            <div class="card-header">
                <h4>Kunde Erstellen</h4>
            </div>
            <div class="card-body">
                <form method="post" action="{{action('CustomerController@update', $customer->id)}}">
                    @method('PATCH')
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="companyName">Firma</label>
                            <input type="text" class="form-control" id="companyName" value="{{$customer->companyName}}" name="companyName">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="contactName">Kontakt Person</label>
                            <input type="text" class="form-control" id="contactName" value="{{$customer->contactName}}" name="contactName">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="addrLine1">Addrese</label>
                        <input type="text" class="form-control" id="addrLine1" value="{{$customer->addrLine1}}" name="addrLine1">
                    </div>
                    <div class="form-group">
                        <label for="addrLine2">Addresse 2</label>
                        <input type="text" class="form-control" id="addrLine2" value="{{$customer->addrLine2}}" name="addrLine2">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">Stadt</label>
                            <input type="text" class="form-control" value="{{$customer->city}}" id="inputCity" name="city">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputZip">PLZ</label>
                            <input type="text" class="form-control" value="{{$customer->zip}}" id="inputZip" name="zip">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputState">Bundesland</label>
                            <input type="text" class="form-control" value="{{$customer->state}}" id="inputState" name="state">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputCountry">Land</label>
                            <input type="text" class="form-control" value="{{$customer->country}}" id="inputCountry" name="country">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="inputCustmerId">Kunden Nr.</label>
                            <input type="text" class="form-control" value="{{$customer->customerId}}" id="inputCustomerId" name="customerId" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputCustomerVAT">Steuer-ID Kunde</label>
                            <input type="text" class="form-control" value="{{$customer->customerVAT}}" name="customerVAT">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Speichern</button>
                </form>
            </div>
        </div>

    </div>

@endsection
