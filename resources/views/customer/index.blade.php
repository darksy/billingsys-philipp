
@extends('layouts.app')

@section('content')

<div class="container">
    @if(count($customers) == 0)
    <div class="card border-warning text-center text-warning">
        <div class="card-header">Fehler</div>
        <div class="card-body">
            <p>Es wurden keine Kunden in der Datenbank gefunden. <a href="/customers/create">Neue Kunden anlegen?</a></p>
        </div>
    </div>

    @else
    <table class="table table-striped">
        <thead>
            <tr>
                <td>ID</td>
                <td>Firma</td>
                <td>Kontakt Person</td>
                <td>Aktionen</td>
            </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr> 
                <td>{{$customer->id}}</td> 
                <td>{{$customer->companyName}}</td>
                <td>{{$customer->contactName}}</td>
                <td>
                    <a href="/customers/{{$customer->id}}/edit"><i class="far fa-edit"></i></a>
                    <a href="#"><i class="far fa-eye"></i></a>
                    <a href="/customers/{{$customer->id}}/delete"><i class="far fa-trash-alt"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>    
    </table>
    @endif
</div>

@endsection