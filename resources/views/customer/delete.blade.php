<?php
/**
 * Created by PhpStorm.
 * User: juhon
 * Date: 13/09/2018
 * Time: 13:53
 */

?>
@extends('layouts.app')

@section('content')


<div class="container">
    <div class="card">
        <div class="card-header">
            Kunde Löschen
        </div>
        <div class="card-body">
            <div class="alert alert-warning">
                Möchten sie diesen Kunden wirklich löschen?
                <form action="{{action('CustomerController@destroy', $customer->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-default" type="submit">Ja, Bitte Löschen</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

